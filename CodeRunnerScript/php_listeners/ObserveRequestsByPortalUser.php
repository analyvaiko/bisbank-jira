/* When request has CustomerRequestType in array ( СДО -> ЕЦП IFOBS)
add user 'Sharipov','Strokanmi','Dubogriiip','Baziuk' to the Request participants list
Otherwise add just 'Dubogriiip','Baziuk' 
11.06.2020 - Убрал Базюка по его просьбе, он будет сам через Бекенд искать*/

//RQ-11115
//* editable */

$observe_config = array(
//	array(
//		'request_types' => array('ANY'), 'observers' => array('mmponomarenko')
//			),
	array(
		'request_types' => array('rq/79131ea9-3d31-418b-8e82-26e18c1d8944'), 'observers' => array('Sharipov') // СДО -> ЕЦП IFOBS
		),
    array(
		'request_types' => array('rq/dcb443be-c077-4b17-8ee5-351c3bf43997'), 'observers' => array('Yevdokymova',) // jetb2 -> тимчасовы права
		),
    array(
		'request_types' => array('rq/57b5cb46-8b21-4f81-b2d0-1d4c89ce24cc'), 'observers' => array('ponomarenkomm','samoylenko','Balanda','Yevdokymova', 'Bespoiasna', 'Kalishuk','Kleister','Serkutannm','Kozlova','Rodionova','Smolnikova','Baziuk','Snigurlp','Gyra','Kravchukom') // jetb2 -> додатковий час
		),
    array(
		'request_types' => array('rq/3fbce1b6-76b1-4cff-8580-90316d1277ce'), 'observers' => array('ponomarenkomm','samoylenko','Balanda','Yevdokymova', 'Bespoiasna', 'Kalishuk','Kleister','Serkutannm','Kozlova','Rodionova','Smolnikova','Baziuk','Snigurlp','Gyra','Kravchukom') // jetb2 -> додатковий час
		),
    array(
		'request_types' => array('rq/ac490efe-b34b-4522-9f78-106be101e1d0'), 'observers' => array('Tymoshenko','Ascheulov','Denysenko','Yevdokymova','Drobiazginmb','Vasylevskyi','Gubanova' ) // jetb2 -> ліміт
		),
    array(
		'request_types' => array('rq/329b7152-4c1b-4149-bd08-4102bc176edc'), 'observers' => array('skrypnykns','bondarenko','rekun') // jetb2 -> ліміт
		),
    array(
		'request_types' => array('rq/79131ea9-3d31-418b-8e82-26e18c1d8944'), 'observers' => array('Strokanmi') // СДО -> ЕЦП IFOBS
		)
);


/* ========================================= Code script ====================================================*/
/*Do not edit following script!!! */

//* Default variables
$adm_username = 'itservicedesk_admin';
$users = null;
$observers = array();

//* Load Atlassian and Java classes and packages section */

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.issue.comments.MutableComment;
import com.atlassian.servicedesk.api.requesttype.RequestType;
import com.atlassian.jira.event.type.EventDispatchOption;
import java.util.*;



//* declare methods */
$customFieldManager = ComponentAccessor::getCustomFieldManager();
$issueManager = ComponentAccessor::getIssueManager();
$commentManager = ComponentAccessor::getCommentManager();
$userManager = ComponentAccessor::getUserManager();


//$issue = $issueManager->getIssueByCurrentKey("RQ-276");
//* get issue properties */
$request_participants = $customFieldManager->getCustomFieldObject("customfield_10000"); // Request participants
$customer_request_type_field = $customFieldManager->getCustomFieldObject("customfield_10001"); // Customer request type

//* get values*/
$adminUser = $userManager->getUserByKey($adm_username);

$issue_customer_request_type = $issue->getCustomFieldValue($customer_request_type_field);

//* process */

//RQ-10190
//if (!empty($issue_request_types) & in_array($issue_customer_request_type , $issue_request_types , false))

foreach($observe_config as &$observe)
{
	if (!empty($observe['request_types']) & !empty($observe['observers'])) 
	{
		if (in_array('ANY' , $observe['request_types'] , false) || in_array($issue_customer_request_type , $observe['request_types'] , false)) 
		{
			    foreach ($observe['observers'] as &$value) 
				{
					array_push($observers,$value);
				}
				unset($value); 
			
		}
	}
		
}
if (!empty($observers))
{
    foreach ($observers as &$value) 
		{	
			$value =  $userManager->getUserByName($value);
        }
    unset($value);	
    $users = $issue->getCustomFieldValue($request_participants);
    if (!empty($users)) 
	{
		$new_user_list = $users;
         }
    else {
        $new_user_list = new ArrayList;
    }
    foreach ($observers as $value) 
    {
        array_push($new_user_list,$value);
    } 
     try {
        $issue->setCustomFieldValue($request_participants, $new_user_list);
        $issueManager->updateIssue($adminUser, $issue, EventDispatchOption::DO_NOT_DISPATCH, false);
     }
     catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), $users,  "\n";
        exit;
     }
};


// --- end ---