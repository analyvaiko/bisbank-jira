import com.atlassian.jira.component.ComponentAccessor;
$customFieldManager = ComponentAccessor::getCustomFieldManager(); // static method
$fieldObj = $customFieldManager->getCustomFieldObject("customfield_10803");
$phone_value = $issue->getCustomFieldValue($fieldObj);

//set POST variables
$phone = '0' . substr($phone_value,strlen($phone_digits)-9,9);
$msg_text = 'Shanovny%20klient,vashe%20pytannya%20z%20BIS%2024%20vyrisheno.%20Dyakuemo%20za%20zvernennenya.';
$url = 'http://esb-jetty-prod:8080/bissms/wapi/Message/Send?Params.Phone='.$phone.'&Params.MessageText='.$msg_text;

//open connection
$ch = curl_init();

//set options
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/json", "X-Bissms-Token: x9Qg4tx8uFQthgmKrRuAXukXRHbkr2Mv"));
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //needed so that the $result=curl_exec() output is the file and isn't just true/false

//execute post
$result = curl_exec($ch);

//close connection
curl_close($ch);

