//* Load Atlassian and Java classes and packages section */

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.java.util.Collection;
import java.util.ArrayList;
import com.atlassian.jira.issue.comments.MutableComment;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.servicedesk.api.requesttype.RequestType;


