import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.bc.user.search.UserSearchParams;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.event.type.EventDispatchOption;
import groovy.sql.Sql
import java.sql.Driver
import java.sql.*
    
def customFieldManager = ComponentAccessor.getCustomFieldManager();
def issueManager = ComponentAccessor.getIssueManager();
def userSearchService = ComponentAccessor.getComponent(UserSearchService.class);  
def userManager = ComponentAccessor.getUserManager();
    
    
// constant values
String USER_NAME = 'itservicedesk_admin';
def default_boss = userManager.getUserByName('Ponomarenkomm');
String driver_class = 'oracle.jdbc.OracleDriver';
String username = 'TECH_JIRA';
String passw = 'Vtkmybrjdf83L';  
String jdbc_str = 'jdbc:oracle:thin:@10.44.0.11:1521/SRBANK';   
Double p_impact_value = 0
//def issue = issueManager.getIssueByCurrentKey("RQ-276"); 
def issue = $issue;
String reporter_name = issue.getReporter().getDisplayName();

// Connect to the database and gain impact value
def driver = Class.forName(driver_class).newInstance() as Driver
def props = new Properties();
def conn = null;
def statement = null
props.setProperty("user", username) 
props.setProperty("password", passw)
try {
	conn = driver.connect(jdbc_str, props) 
	statement = conn.prepareCall("{call SR_BANK.P_JIRA.P_JIRA_IMPACT(?, ?, ?, ?)}")
    statement.setString(1, reporter_name);
    statement.setString(2, issue.getKey());
    statement.registerOutParameter(3, oracle.jdbc.driver.OracleTypes.NUMBER);
    statement.registerOutParameter(4, oracle.jdbc.driver.OracleTypes.VARCHAR);
    statement.execute();
    p_impact_value = statement.getInt(3).toDouble();
	boss_value = statement.getString(4);
}
 finally {
        //close the sql statements
        if (statement != null)
            statement.close();
		if (conn != null)
			conn.close();
}


// update current issue 

def adminUser = userManager.getUserByKey(USER_NAME);    
def p_jira_impact_field = customFieldManager.getCustomFieldObject("customfield_11000"); //P_JIRA_IMPACT custom field
def boss_field = customFieldManager.getCustomFieldObject("customfield_10403"); //BOSS custom field

issue.setCustomFieldValue(p_jira_impact_field, p_impact_value);

def boss_arr = userSearchService.findUsersByFullName(boss_value);
if (issue.getCustomFieldValue(boss_field)) {}
else {
	if (boss_arr.size()) {
			boss_user	= userSearchService.findUsersByFullName(boss_value).get(0);
 
		}
	else {
			boss_user = default_boss;
		}
	try {
		issue.setCustomFieldValue(boss_field, boss_user);
		issueManager.updateIssue(adminUser, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
	}
	finally {}
}