import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.config.StatusManager
import com.atlassian.jira.config.IssueTypeManager
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.workflow.edit.Workflow
import com.opensymphony.workflow.loader.StepDescriptor

String err_msg = '';
String new_issue_type_id;

def issueManager = ComponentAccessor.getIssueManager();
def customFieldManager = ComponentAccessor.getCustomFieldManager();
def projectManager = ComponentAccessor.getProjectManager();
def wfManager = ComponentAccessor.getWorkflowManager()
def statusManager = ComponentAccessor.getComponent(StatusManager.class)
def issueTypeManager = ComponentAccessor.getComponent(IssueTypeManager.class)

Issue issue = issueManager.getIssueByCurrentKey("TEST-1");
def currentProject = projectManager.getProjectByCurrentKey("RQ")

String choosed_issue_type_name = issue.getCustomFieldValue(customFieldManager.getCustomFieldObject("customfield_11203"));

new_issue_type_id = getNewIssueTypeId(choosed_issue_type_name);
/*
if (issue.getIssueTypeId() == new_issue_type_id | new_issue_type_id == null) {
    return null
}
*/
def new_issue_type = issueTypeManager.getIssueType(new_issue_type_id)

issue.setIssueTypeObject(new_issue_type)

/*
wfManager.migrateIssueToWorkflow(
issue,
wfManager.getWorkflow("RQ: inc-prb workflow for Jira Service Desk"),
statusManager.getStatus("10002")
)
*/


def targetWorkflow = wfManager.getWorkflow(currentProject.getId(),new_issue_type_id)
return getTargetStatus(issue,targetWorkflow)

private String getNewIssueTypeId(String name) {
    if (name == null) {
        return null
    } else return getReservedIssueTypes().get(name);
}

private HashMap getReservedIssueTypes() {
    Map<String,String> issueTypesMap = new HashMap();
    issueTypesMap.put("Incident","10002");
    issueTypesMap.put("Problem","10006");
    issueTypesMap.put("Service Request","10003");
    issueTypesMap.put("Service Request with Approvals","10004");
    issueTypesMap.put("Change","10005");
    return issueTypesMap;
}

def getLinkedWorkflow(def project,String issueTypeId) {
    def wfSchemeManager = ComponentAccessor.getWorkflowSchemeManager();
    def wfManager = ComponentAccessor.getWorkflowManager()
    String workflowName= wfSchemeManager.getWorkflowMap(project).get(issueTypeId);
    if (workflowName == null) {
        workflowName= wfSchemeManager.getWorkflowMap(project).get(null);
    }
    return wfManager.getWorkflow(workflowName);
}

def getTargetStatus(def currentIssue, def targetWorkflow) {
    String currentStatusId = currentIssue.getStatusId();
    Set<String> linkedStatusIds = targetWorkflow.getLinkedStatusIds()
    if (linkedStatusIds.contains(currentStatusId)) {
        return currentStatusId
    } else {
        def actions = targetWorkflow.getActionsByName("Create issue");
        Iterator iterator = actions.iterator();
        while (iterator.hasNext()) {
            final Collection<StepDescriptor> steps =  targetWorkflow.getStepsForTransition(iterator.next())
            if (!steps.isEmpty()) {
                final StepDescriptor step = steps.iterator().next();
                if (step.getAction(transition.getId()) != null) {
                    return step.getId();
                }
            }




        }
    }
}
/*


def newIssueType = issueTypeManager.getIssueType("10200")

issue.setIssueTypeObject(newIssueType)

wfManager.migrateIssueToWorkflow(
issue,
wfManager.getWorkflow("RQ: inc-prb workflow for Jira Service Desk"),
statusManager.getStatus("10002")
)

DefaultIssueChangeHolder changeHolder = new DefaultIssueChangeHolder()

// Request Type Custom Field
def serviceType = "customfield_10202"

def customFieldTarget = ComponentAccessor.customFieldManager.getCustomFieldObject(serviceType)

def requestType = customFieldTarget.getCustomFieldType().getSingularObjectFromString("hr/e5f939ee-6472-4357-b6bd-e1dbfdc9390b")

customFieldTarget.updateValue(null, issue, new ModifiedValue(issue.getCustomFieldValue(customFieldTarget), requestType), changeHolder)


*/