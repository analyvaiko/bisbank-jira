import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.event.type.EventDispatchOption;
import java.util.concurrent.ThreadLocalRandom;

// constant values
String USER_NAME = 'itservicedesk_admin';

def customFieldManager = ComponentAccessor.getCustomFieldManager();
def optionsManager = ComponentAccessor.getOptionsManager();
def issueManager = ComponentAccessor.getIssueManager();
def userManager = ComponentAccessor.getUserManager();

//Issue issue = issueManager.getIssueByCurrentKey("RQ-276");
Issue issue = $issue;

def rnd_field = customFieldManager.getCustomFieldObject("customfield_11026");

def options = optionsManager.getOptions(rnd_field.getRelevantConfig(issue));
if (options.size() > 0) {
	def rnd_value = ThreadLocalRandom.current().nextInt(0, options.size());


	issue.setCustomFieldValue(rnd_field, options[rnd_value]);

	try {
		issueManager.updateIssue(userManager.getUserByKey(USER_NAME), issue, EventDispatchOption.DO_NOT_DISPATCH, false);
		}
	finally {}
}